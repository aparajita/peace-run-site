module.exports = {
  parser: 'babel-eslint',

  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
  },

  env: {
    browser: true,
  },

  extends: ['airbnb-base', 'prettier'],

  plugins: ['import', 'prettier'],

  rules: {
    'class-methods-use-this': 'off',
    curly: ['error', 'all'],
    'func-names': 'off',
    'no-buffer-constructor': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    'no-path-concat': 'off',
    'no-prototype-builtins': 'off',
    'no-plusplus': 'off',
    'no-restricted-syntax': 'off',
    'no-underscore-dangle': ['error', { allowAfterThis: true }],
    'prefer-destructuring': 'off',
    'prefer-rest-params': 'off',
    'prefer-spread': 'off',
    'prettier/prettier': ['error'],
  },

  overrides: [
    {
      files: ['gulpfile.js'],
      rules: {
        'import/no-dynamic-require': 'off',
      },
    },
  ],
};

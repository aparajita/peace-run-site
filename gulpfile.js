/* eslint global-require: "off" */
const gulp = require('gulp');

const debug = process.argv.includes('--debug', 3);

const paths = {
  html: {
    src: ['src/pages/**/*.{ejs,html}', '!src/**/includes/**/*.{ejs,html}'],
    watch: ['src/pages/**/*.{ejs,html}', 'src/pages/**/data.js'],
    dest: 'dist',
  },

  js: {
    compile: {
      src: ['src/pages/**/*.js', 'src/assets/js/*.js', '!src/pages/**/data.js'],
      dest: 'dist/assets/js',
    },
    copy: {
      src: 'src/assets/js/vendor/*.js',
      dest: 'dist/assets/js/vendor',
    },
    watch: ['src/pages/**/*.js', 'src/assets/js/*.js', '!src/pages/**/data.js'],
  },

  css: {
    src: ['src/assets/css/styles.scss'],
    watch: [
      'src/assets/css/**/*.?(s)css',
      'src/pages/**/*.?(s)css',
      './tailwind.js',
      './tailwind-plugins/*.js',
    ],
    dest: 'dist/assets/css',
    vendor: {
      src: ['src/assets/css/vendor/*.css'],
      dest: 'dist/assets/css/vendor',
    },
  },

  img: {
    src: 'src/assets/img/**/*',
    dest: 'dist/assets/img',
  },

  favicon: {
    src: 'src/assets/img/favicons/*',
    dest: 'dist',
  },
};

let production = false;

function setProduction(done) {
  production = true;
  done();
}

function productionOnly(plugin) {
  return production ? require(plugin) : require('through2').obj;
}

let changed;
let flatten;
let gulpif;
let pump;
let sourcemaps;
let uglify;

function setupCompile() {
  changed = require('gulp-changed');
  flatten = require('gulp-flatten');
  gulpif = require('gulp-if');
  pump = require('pump');
  sourcemaps = require('gulp-sourcemaps');
  uglify = productionOnly('gulp-uglify');
}

function clean() {
  const del = require('del');

  return del(['dist/**/*', 'build']);
}

function cleanJs() {
  const del = require('del');

  return del(['dist/**/*.js']);
}

function cleanCss() {
  const del = require('del');

  return del(['dist/**/*.css']);
}

function cleanHtml() {
  const del = require('del');

  return del(['dist/**/*.html']);
}

function cleanImg() {
  const del = require('del');

  return del(['dist/assets/img/*']);
}

function cleanFavicon() {
  const del = require('del');

  return del([
    'dist/android-*.png',
    'dist/apple-touch-*.png',
    'dist/browserconfig.xml',
    'dist/favicon*',
    'dist/mstile-*.png',
    'dist/safari-pinned-tab.svg',
    'dist/site.webmanifest',
  ]);
}

function copyJs() {
  return gulp.src(paths.js.copy.src).pipe(gulp.dest(paths.js.copy.dest));
}

function compileJs(done) {
  const babel = require('gulp-babel');

  setupCompile();

  pump(
    [
      gulp.src(paths.js.compile.src),
      changed(paths.js.compile.dest),
      babel(),
      gulpif(production, uglify()),
      flatten(),
      gulp.dest(paths.js.compile.dest),
    ],
    done
  );
}

function copyVendorCss() {
  return gulp.src(paths.css.vendor.src).pipe(gulp.dest(paths.css.vendor.dest));
}

function compileCss(done) {
  const globby = require('globby');
  const nodeSass = require('node-sass');
  const postcss = require('gulp-postcss');
  const purgeHtml = require('purgecss-from-html');
  const purgecss = productionOnly('gulp-purgecss');
  const sass = require('gulp-sass');
  const tailwind = require('tailwindcss');

  // so we clear them from the cache.
  const tailwindFiles = globby
    .sync(['./tailwind.js', './tailwind-plugins/*.js'])
    .map(file => require.resolve(file));

  // Tailwind require's files, if we are watching they are cached,
  tailwindFiles.forEach(file => delete require.cache[file]);

  const plugins = [tailwind('./tailwind.js'), require('autoprefixer')];

  if (production) {
    plugins.push(require('cssnano'));
  }

  setupCompile();
  sass.compiler = nodeSass;

  const stream = [
    gulp.src(paths.css.src),
    gulpif(debug || !production, sourcemaps.init()),
    sass(),
    postcss(plugins),
    purgecss({
      content: ['dist/*.html'],
      extractors: [
        {
          extractor: purgeHtml,
          extensions: ['html'],
        },
      ],
      whitelistPatterns: [/^pr-/, /^transition-state-/],
    }),
    flatten(),
    gulpif(debug || !production, sourcemaps.write('.')),
    gulp.dest(paths.css.dest),

    // Make a copy in a non-excluded directory so WebStorm can index it
    gulp.dest('build'),
  ];

  pump(stream, done);
}

function compileHtml(done) {
  const clear = require('clear-module');
  const ejs = require('gulp-ejs');
  const htmlmin = productionOnly('gulp-htmlmin');

  setupCompile();

  // Make sure the data is reloaded during a watch
  clear.match(/.+\/src\/pages\/.+/);

  const dataFiles = {
    global: './src/pages/data',
    utils: './ejs-utils',
    home: './src/pages/home/data',
  };

  const data = {};

  Object.keys(dataFiles).forEach(key => {
    const file = dataFiles[key];
    delete require.cache[require.resolve(file)];
    data[key] = require(file);
  });

  pump(
    [
      gulp.src(paths.html.src),
      ejs({ data }, {}, { ext: '.html' }),
      htmlmin({ collapseWhitespace: !debug }),
      flatten(),
      gulp.dest(paths.html.dest),
    ],
    done
  );
}

function copyFavicons() {
  return gulp.src(paths.favicon.src).pipe(gulp.dest(paths.favicon.dest));
}

function compileImg() {
  return gulp.src(paths.img.src).pipe(gulp.dest(paths.img.dest));
}

let bs = null;

function serve(done) {
  if (!bs) {
    bs = require('browser-sync');
  }

  bs({
    server: {
      baseDir: 'dist',
    },
    open: false,
    name: 'server',
  });

  gulp.watch(['*.html', 'assets/**/*'], { cwd: 'dist' }, cb => {
    bs.reload();
    cb();
  });

  done();
}

function watcher(done) {
  Object.keys(paths).forEach(extension => {
    const watchPaths = paths[extension].watch || paths[extension].src;
    const action = extension === 'css' ? 'recompile' : 'compile';

    // Wait a second before triggering to allow for eslint fixing on save
    const options = { delay: 1000 };
    gulp.watch(watchPaths, options, gulp.series(`${action}.${extension}`));
  });

  if (done) {
    done();
  }
}

function deploy(done) {
  const log = require('fancy-log');
  const process = require('child_process');

  const result = process.spawnSync(
    '/usr/local/bin/netlify',
    ['deploy', '-p', 'dist'],
    { encoding: 'utf-8' }
  );

  if (result.status === 0) {
    log(result.stdout);
  } else {
    log(result.stderr || result.stdout);
  }

  done();
}

const taskExtensions = ['img', 'favicon', 'js', 'html', 'css'];

gulp.task('clean.js', cleanJs);
gulp.task('clean.css', cleanCss);
gulp.task('clean.html', cleanHtml);
gulp.task('clean.img', cleanImg);
gulp.task('clean.favicon', cleanFavicon);

gulp.task('compile.js', gulp.parallel(compileJs, copyJs));
gulp.task('compile.css', gulp.parallel(copyVendorCss, compileCss));
// When html changes, the purged CSS may change
gulp.task('compile.html', gulp.series(compileHtml, 'compile.css'));
gulp.task('compile.ejs', compileHtml);
gulp.task('compile.img', compileImg);
gulp.task('compile.favicon', copyFavicons);

gulp.task(
  'compile',
  // Compile in a series because build steps have dependencies.
  // Don't compile css, it's compiled by html.
  gulp.series(
    taskExtensions
      .filter(extension => extension !== 'css')
      .map(extension => `compile.${extension}`)
  )
);

gulp.task(
  'compile-no-img',
  // Compile in a series because build steps have dependencies.
  // Don't compile css, it's compiled by html.
  gulp.series(
    taskExtensions
      .filter(extension => extension !== 'css' && extension !== 'img')
      .map(extension => `compile.${extension}`)
  )
);

taskExtensions.forEach(extension => {
  gulp.task(
    `recompile.${extension}`,
    gulp.series(`clean.${extension}`, `compile.${extension}`)
  );
});

gulp.task('recompile', gulp.series(clean, 'compile'));

taskExtensions.forEach(extension => {
  gulp.task(
    `build.${extension}`,
    gulp.series(setProduction, `recompile.${extension}`)
  );
});

gulp.task('build', gulp.series(setProduction, 'recompile'));
gulp.task('rebuild', gulp.series(clean, 'build'));

gulp.task('deploy', gulp.series('build', deploy));
gulp.task('redeploy', gulp.series('rebuild', deploy));

const dev = gulp.series(serve, watcher);
const redev = gulp.series('recompile', dev);
const watch = gulp.series(watcher);

gulp.task('watch.compile', gulp.series('compile', watch));
gulp.task('watch.recompile', gulp.series('recompile', watch));

module.exports = {
  clean,
  serve,
  watch,
  dev,
  redev,
};

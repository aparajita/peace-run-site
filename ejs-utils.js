module.exports = {
  iterate(array, fn) {
    const last = array.length - 1;

    Array.prototype.forEach.call(array, (element, index) => {
      fn(element, index === last, index === 0, index, array);
    });
  },
};

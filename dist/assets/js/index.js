"use strict";

/*
*   Rational Investment Management - home page
*/

/* eslint-disable no-param-reassign */
// eslint-disable-next-line no-unused-vars
var animate = function () {
  // Assuming we have 30fps, this is one frame in milliseconds
  var frameDuration = 1000 / 30;

  function animateElement(_ref) {
    var el = _ref.el,
        duration = _ref.duration,
        run = _ref.run,
        data = _ref.data;
    var ms = 1000 * duration;
    var end = Date.now() + ms;

    var step = function step() {
      var current = Date.now();
      var remaining = end - current;
      var progress = 1 - remaining / ms;

      if (remaining < frameDuration) {
        run(el, progress, data); // 1 = progress is at 100%

        return;
      }

      run(el, progress, data);
      requestAnimationFrame(step);
    };

    step();
  }

  function offsetMe(el, progress, data) {
    if (data.offset >= 0 && data.offset <= data.length) {
      data.offset = data.length * progress;
      el.style.strokeDashoffset = data.length - data.offset;
    }
  }

  function animateLine(line, duration) {
    var el = typeof line === 'string' ? document.querySelector(line) : line;
    var pathLength = el.getTotalLength();
    el.style.strokeDasharray = pathLength;
    el.style.strokeDashoffset = pathLength;
    setTimeout(function () {
      el.style.visibility = 'visible';
      animateElement({
        el: el,
        duration: duration,
        run: offsetMe,
        data: {
          offset: 0,
          length: pathLength
        }
      });
    }, 0);
  }

  return {
    animateLine: animateLine
  };
}();

(function () {
  function fixScrollCuePosition() {
    /*
      On iOS Safari, the hero is 100vh, which extends beyond
      the bottom of the viewport. The scroll cue is positioned
      relative to the viewport bottom, so we move it up
      by the overhang.
    */
    var heroHeight = document.getElementById('hero').getBoundingClientRect().height;
    var heightDiff = heroHeight - window.innerHeight;

    if (heightDiff !== 0) {
      var cue = document.getElementById('hero-scroll-cue'); // eslint-disable-next-line no-param-reassign

      cue.style.cssText += " bottom: ".concat(heightDiff, "px;");
    }
  }

  function initNavbarScrollActions() {
    var navbar = document.querySelector('header');
    var navbarWatcher = scrollMonitor.create('#hero', -navbar.clientHeight);
    navbarWatcher.enterViewport(function () {
      // Hero is at the bottom of the navbar
      navbar.classList.add('navbar--hero');
    });
    navbarWatcher.exitViewport(function () {
      // Hero is above the bottom of the navbar
      navbar.classList.remove('navbar--hero');
    });
  }

  window.addEventListener('DOMContentLoaded', function () {
    fixScrollCuePosition();
    initNavbarScrollActions();
  }, false);
})();
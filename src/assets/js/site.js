/* eslint-disable no-param-reassign */

// eslint-disable-next-line no-unused-vars
function getElementData(el, name) {
  if (el.dataset) {
    const camelCase = name.replace(/-([a-z])/gi, match =>
      match[1].toUpperCase()
    );
    return el.dataset[camelCase];
  }

  return el.getAttribute(`data-${name}`);
}

(function() {
  const { forEach } = Array.prototype;

  function initMenus() {
    const menuTriggers = document.querySelectorAll('.pr-menu-trigger');
    let currentOpenMenu = null;

    const closeMenuHandler = event => {
      // If this was a touchstart on any part of the menu, ignore it
      // and we'll handle it when it becomes a mousedown.
      const classes = event.target.className;

      if (event.type === 'touchstart' && /\brim-menu-\w+\b/.test(classes)) {
        return;
      }

      if (
        // If this event was in the menu content, don't let it propogate
        // up but do let the default action occur so links will work.
        event.target.classList.contains('pr-menu-content') ||
        event.target.classList.contains('pr-menu-item')
      ) {
        event.stopPropagation();
      } else {
        // If this event was not in the menu content, close the menu
        toggleMenu(currentOpenMenu); // eslint-disable-line no-use-before-define
        currentOpenMenu = null;
      }
    };

    const toggleMenu = function(menu) {
      // Toggle the hiliting of the menu trigger
      menu.classList.toggle('pr-menu--open');

      // Open/close the menu
      const menuClasses = menu.nextElementSibling.classList;
      menuClasses.toggle('transition-state-normal');
      menuClasses.toggle('pointer-events-none');

      if (currentOpenMenu) {
        // If we just closed an open menu, there is no longer
        // need to listen for events that might close it.
        window.removeEventListener('mousedown', closeMenuHandler, false);
        window.removeEventListener('touchstart', closeMenuHandler, false);
      }
    };

    const menuTriggerHandler = event => {
      if (!currentOpenMenu) {
        // If a menu was clicked and it isn't the current open menu, open it
        toggleMenu(event.target);
        currentOpenMenu = event.target;

        // Stop this event
        event.stopPropagation();
        event.preventDefault();

        // Add listeners to the window so we can close the menu if
        // something other than the menu is clicked/tapped.
        window.addEventListener('mousedown', closeMenuHandler, false);
        window.addEventListener('touchstart', closeMenuHandler, false);
      } else {
        // If a menu currently is open, close it
        toggleMenu(currentOpenMenu);
        currentOpenMenu = null;
      }
    };

    const menuContentHandler = event => {
      // We are handling the event, don't propagate
      event.stopPropagation();

      // If a menu item link was clicked, close the menu
      if (event.target.tagName.toLowerCase() === 'a') {
        toggleMenu(currentOpenMenu);
        currentOpenMenu = null;
      }
    };

    // Set up the menu events
    forEach.call(menuTriggers, trigger => {
      trigger.addEventListener('mousedown', menuTriggerHandler, false);
    });

    const menus = document.querySelectorAll('.pr-menu-content');

    forEach.call(menus, menu => {
      menu.addEventListener('click', menuContentHandler, false);
    });
  }

  function setupScrollActions() {
    const scrollElements = document.querySelectorAll('[data-scroll-animation]');

    forEach.call(scrollElements, element => {
      const animation = getElementData(element, 'scroll-animation');

      if (!animation) {
        return;
      }

      const height = element.clientHeight;
      let offset = 0;
      const scrollOffset = getElementData(element, 'scroll-offset');

      if (scrollOffset != null) {
        const factor = parseFloat(scrollOffset);
        offset = -Math.floor(height * factor);
      }

      // Trigger the transition when the element is visible
      scrollMonitor
        .create(element, { bottom: offset })
        .fullyEnterViewport(() => {
          element.style.cssText += ` animation: ${animation};`;
        }, true); // Only do this once, pass true
    });
  }

  function initLoadActions() {
    const elements = document.querySelectorAll('[data-load-animation]');

    forEach.call(elements, element => {
      const animation = getElementData(element, 'load-animation');
      // eslint-disable-next-line no-param-reassign
      element.style.cssText += ` animation: ${animation}`;
    });
  }

  document.body.classList.add('pr-content-loading');

  window.addEventListener(
    'DOMContentLoaded',
    () => {
      initMenus();
    },
    false
  );

  window.addEventListener(
    'load',
    () => {
      // The page is completely loaded, remove the class
      // that pauses all animation
      document.body.classList.remove('pr-content-loading');
      setupScrollActions();
      initLoadActions();
    },
    false
  );
})();

/* eslint-enable */

module.exports = {
  navLinks: {
    home: {
      title: 'Home',
      href: '/',
    },

    about: {
      title: 'About',
      href: '/about.html',
    },

    contact: {
      title: 'Contact',
      href: '/contact.html',
    },
  },

  quadEaseOut: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
};

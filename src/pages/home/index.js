/*
*   Rational Investment Management - home page
*/

/* eslint-disable no-param-reassign */

// eslint-disable-next-line no-unused-vars
const animate = (function() {
  // Assuming we have 30fps, this is one frame in milliseconds
  const frameDuration = 1000 / 30;

  function animateElement({ el, duration, run, data }) {
    const ms = 1000 * duration;
    const end = Date.now() + ms;

    const step = () => {
      const current = Date.now();
      const remaining = end - current;
      const progress = 1 - remaining / ms;

      if (remaining < frameDuration) {
        run(el, progress, data); // 1 = progress is at 100%
        return;
      }

      run(el, progress, data);
      requestAnimationFrame(step);
    };

    step();
  }

  function offsetMe(el, progress, data) {
    if (data.offset >= 0 && data.offset <= data.length) {
      data.offset = data.length * progress;
      el.style.strokeDashoffset = data.length - data.offset;
    }
  }

  function animateLine(line, duration) {
    const el = typeof line === 'string' ? document.querySelector(line) : line;
    const pathLength = el.getTotalLength();

    el.style.strokeDasharray = pathLength;
    el.style.strokeDashoffset = pathLength;

    setTimeout(() => {
      el.style.visibility = 'visible';

      animateElement({
        el,
        duration,
        run: offsetMe,
        data: {
          offset: 0,
          length: pathLength,
        },
      });
    }, 0);
  }

  return { animateLine };
})();

(function() {
  function fixScrollCuePosition() {
    /*
      On iOS Safari, the hero is 100vh, which extends beyond
      the bottom of the viewport. The scroll cue is positioned
      relative to the viewport bottom, so we move it up
      by the overhang.
    */
    const heroHeight = document.getElementById('hero').getBoundingClientRect()
      .height;

    const heightDiff = heroHeight - window.innerHeight;

    if (heightDiff !== 0) {
      const cue = document.getElementById('hero-scroll-cue');

      // eslint-disable-next-line no-param-reassign
      cue.style.cssText += ` bottom: ${heightDiff}px;`;
    }
  }

  function initNavbarScrollActions() {
    const navbar = document.querySelector('header');
    const navbarWatcher = scrollMonitor.create('#hero', -navbar.clientHeight);

    navbarWatcher.enterViewport(() => {
      // Hero is at the bottom of the navbar
      navbar.classList.add('navbar--hero');
    });

    navbarWatcher.exitViewport(() => {
      // Hero is above the bottom of the navbar
      navbar.classList.remove('navbar--hero');
    });
  }

  window.addEventListener(
    'DOMContentLoaded',
    () => {
      fixScrollCuePosition();
      initNavbarScrollActions();
    },
    false
  );
})();

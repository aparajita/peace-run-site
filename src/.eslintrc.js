module.exports = {
  env: {
    browser: true,
  },

  globals: {
    scrollMonitor: true,
    TimelineLite: true,
    TweenLite: true,
    TweenMax: true,
    GSDevTools: true,
    Power0: true,
    Power1: true,
    Power2: true,
    Power3: true,
    Power4: true,
    Back: true,
    Elastic: true,
    Bounce: true,
  },

  rules: {
    'import/no-dynamic-require': 'error',
    'import/no-extraneous-dependencies': 'error',
    'global-require': 'error',
    'no-restricted-syntax': 'error',
  },
};
